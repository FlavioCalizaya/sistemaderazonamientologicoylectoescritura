﻿# Host: localhost:3307  (Version 5.5.5-10.1.37-MariaDB)
# Date: 2019-05-29 15:21:59
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "evaluacion"
#

DROP TABLE IF EXISTS `evaluacion`;
CREATE TABLE `evaluacion` (
  `idEvaluacion` int(11) NOT NULL,
  `prueba` varchar(70) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `contenido_idcontenido` int(11) NOT NULL,
  PRIMARY KEY (`idEvaluacion`,`contenido_idcontenido`),
  KEY `fk_Evaluacion_contenido1_idx` (`contenido_idcontenido`),
  CONSTRAINT `fk_Evaluacion_contenido1` FOREIGN KEY (`contenido_idcontenido`) REFERENCES `contenido` (`idcontenido`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "evaluacion"
#


#
# Structure for table "rol"
#

DROP TABLE IF EXISTS `rol`;
CREATE TABLE `rol` (
  `idRol` int(11) NOT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idRol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "rol"
#

INSERT INTO `rol` VALUES (1,'Administrador'),(2,'UsuarioRegular');

#
# Structure for table "perfilusuario"
#

DROP TABLE IF EXISTS `perfilusuario`;
CREATE TABLE `perfilusuario` (
  `idperfilUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(45) NOT NULL,
  `primerApellido` varchar(45) NOT NULL,
  `segundoApellido` varchar(45) DEFAULT NULL,
  `ocupacion` varchar(45) DEFAULT NULL,
  `nombreNiño` varchar(45) DEFAULT NULL,
  `cursoNiño` varchar(45) DEFAULT NULL,
  `fechaRegistro` datetime DEFAULT CURRENT_TIMESTAMP,
  `fechaActualizacion` datetime DEFAULT CURRENT_TIMESTAMP,
  `Rol_idRol` int(11) NOT NULL DEFAULT '2',
  PRIMARY KEY (`idperfilUsuario`,`Rol_idRol`),
  KEY `fk_perfilUsuario_Rol1_idx` (`Rol_idRol`),
  CONSTRAINT `fk_perfilUsuario_Rol1` FOREIGN KEY (`Rol_idRol`) REFERENCES `rol` (`idRol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

#
# Data for table "perfilusuario"
#

INSERT INTO `perfilusuario` VALUES (1,'Marcelo','Martinez',NULL,'Estudiante',NULL,NULL,'2019-05-28 11:19:35','2019-05-28 11:19:35',1);

#
# Structure for table "reporteevaluacion"
#

DROP TABLE IF EXISTS `reporteevaluacion`;
CREATE TABLE `reporteevaluacion` (
  `idreporteEvaluacion` int(11) NOT NULL,
  `puntaje` int(11) DEFAULT NULL,
  `perfilUsuario_idperfilUsuario` int(11) NOT NULL,
  `Evaluacion_idEvaluacion` int(11) NOT NULL,
  PRIMARY KEY (`idreporteEvaluacion`,`perfilUsuario_idperfilUsuario`,`Evaluacion_idEvaluacion`),
  KEY `fk_reporteEvaluacion_perfilUsuario1_idx` (`perfilUsuario_idperfilUsuario`),
  KEY `fk_reporteEvaluacion_Evaluacion1_idx` (`Evaluacion_idEvaluacion`),
  CONSTRAINT `fk_reporteEvaluacion_Evaluacion1` FOREIGN KEY (`Evaluacion_idEvaluacion`) REFERENCES `evaluacion` (`idEvaluacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reporteEvaluacion_perfilUsuario1` FOREIGN KEY (`perfilUsuario_idperfilUsuario`) REFERENCES `perfilusuario` (`idperfilUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "reporteevaluacion"
#


#
# Structure for table "contenido"
#

DROP TABLE IF EXISTS `contenido`;
CREATE TABLE `contenido` (
  `idcontenido` int(11) NOT NULL DEFAULT '0',
  `nombreArea` varchar(50) DEFAULT NULL,
  `descripcion` varchar(150) DEFAULT NULL,
  `fechResgisto` datetime DEFAULT CURRENT_TIMESTAMP,
  `perfilUsuario_idperfilUsuario` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idcontenido`,`perfilUsuario_idperfilUsuario`),
  KEY `fk_contenido_perfilUsuario1_idx` (`perfilUsuario_idperfilUsuario`),
  CONSTRAINT `fk_contenido_perfilUsuario1` FOREIGN KEY (`perfilUsuario_idperfilUsuario`) REFERENCES `perfilusuario` (`idperfilUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Data for table "contenido"
#

INSERT INTO `contenido` VALUES (1,'Logica','trabaja la parte logica','2019-05-29 15:20:29',1);
