<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class estudiantes_model extends CI_Model {

	public function  retornarAu()
	{
        $estado1=1;
        $this->db->select('*');
        $this->db->from('usuario');
        $this->db->where('estado',$estado1);
       // $this->db->order_by('idestudiante');
        return $this->db->get();

	
    }
  public function agregarU($ps)
  {
    $this->db->insert('usuario',$ps);
    
  }

  public function recuperarU($idUsuario)
  {
    $this->db->select('*');
    $this->db->from('usuario');
    $this->db->where('idUsuario',$idUsuario);
    return $this->db->get();
  }
  public function retornarCu()
  {
    $estado1=1;
    $this->db->select('*');
    $this->db->from('curso');
    $this->db->where('estado',$estado1);
   // $this->db->order_by('idestudiante');
    return $this->db->get();

  }

  public function retornarCuenta($cuenta)
  {
    $this->db->select('*');
    $this->db->from('usuario');
    $this->db->where('cuenta',$cuenta);
    return $this->db->get();
  }

  public function modificarU($idUsuario,$ps)
  {
    $this->db->where('idUsuario',$idUsuario);
    $this->db->update('usuario',$ps);
  }

  public function eliminarU($idperfilUsuario,$ps)
  {
    $this->db->where('idUsuario',$idperfilUsuario);
    $this->db->update('usuario',$ps);
    
  }

  public function  retornarCo()
  {
        $this->db->select('*');
        $this->db->from('contenido');
       // $this->db->order_by('idestudiante');
        return $this->db->get();
  
    }
      public function agregarC($c)
  {
    $this->db->insert('contenido',$c);
    
  }
  public function recuperarC($idcontenido)
  {
    $this->db->select('*');
    $this->db->from('contenido');
    $this->db->where('idcontenido',$idcontenido);
    return $this->db->get();
  }

  public function modificarC($idcontenido,$c)
  {
    $this->db->where('idcontenido',$idcontenido);
    $this->db->update('contenido',$c);
  }
public function eliminarC($idcontenido)
  {
    $this->db->where('idcontenido',$idcontenido);
    $this->db->delete('contenido');
    
  }

}