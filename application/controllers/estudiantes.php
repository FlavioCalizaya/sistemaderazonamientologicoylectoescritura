<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estudiantes extends CI_Controller {

	
 public function index()//cargar login
  {
        
        $this->load->view('vistaInicio');



  }

   public function index2()//cargar inicio
  {
      //  $data['estudiantes']=$this->estudiantes_model->retornarEstudiantes();

        $this->load->view('head');
        $this->load->view('vistaLista');



  }
  public function curso()//cargar inicio
  {
      //  $data['estudiantes']=$this->estudiantes_model->retornarEstudiantes();
      $data['perfilusuario']=$this->estudiantes_model->retornarCu();
        $this->load->view('headCursos');
        $this->load->view('vistaCurso',$data);



  }
	public function Notas()//cargar registro
	{
        $data['perfilusuario']=$this->estudiantes_model->retornarAu();

        $this->load->view('head');
        $this->load->view('vistanotas',$data);
    }
    public function agregar()//agregar usuario
  {
    
    
    $this->load->view('headRegistro');
    $this->load->view('agregarform');
    //$this->load->view('footer');
  }

  public function iniSesion()//agregar usuario
  {
    
    
    $this->load->view('headIniSesion');
    $this->load->view('iniciarform');
    //$this->load->view('footer');
  }

  public function iniSesiondb()
  {
   
    $cuenta=$this->input->post('cuenta');
    $contrasenia=$this->input->post('contrasenia');
    $data['usuarios']=$this->estudiantes_model->retornarCuenta($cuenta);
    if ($data->result!=null)
    {
      foreach ($data->result() as $row) {
         
          
              if ($contrasenia==$row->contrasenia)
             {
                echo "bienvenido";
             }
              else
              {  
                echo "contraseña incorrecrta";
              }
              
        }
  
    }
    else
      {
         echo "no existe esa cuenta";
      }
    
    //$pais=$_POST['pais'];
    //$data['pais']=$pais;
    //$this->paises_model->agregarPais($data);
    

   // $this->load->view('footer');

  }

  public function agregardb()
  {
    $nombre=$this->input->post('nombre');
    $apellidoP=$this->input->post('apellidoP');
    $apellidoM=$this->input->post('apellidoM');
    $cuenta=$this->input->post('cuenta');
    $contrasenia=$this->input->post('contrasenia');
    $contrasenia2=$this->input->post('contrasenia2');
    
    //$pais=$_POST['pais'];
    //$data['pais']=$pais;
    //$this->paises_model->agregarPais($data);
    if ($nombre!="" )
    {
      if($apellidoP!="")
      {
        if($cuenta!="")
        {
          if($contrasenia==$contrasenia2){
            $ps = array('nombre' =>$this->input->post('nombre'),'apellidoP' =>$this->input->post('apellidoP'),'apellidoM' =>$this->input->post('apellidoM'),'cuenta' =>$this->input->post('cuenta'),'contrasenia' =>$this->input->post('contrasenia'));
            $this->estudiantes_model->agregarU($ps);
       $this->load->view('head');
       $this->load->view('agregarmensaje',$ps);
          }
          else{
            echo "las contraseñas no coinciden";
          }
  
        }
          else
          {
            echo "el campo de cuenta de usuario no puede estar vacio";
          }
      }
      else{
        echo "el campo del primer apellido no puede estar vacio";
      }
    }
    else{
      echo "el campo del nombre no puede estar vacio";
    }

   // $this->load->view('footer');

  }
    public function registro()
	{
              
              $this->load->view('head');
               $this->load->view('vistaregistro');     

	}

  public function modificar()
  {
    $idUsuario=$_POST['idUsuario'];
    $data['nombre']=$this->estudiantes_model->recuperarU($idUsuario);
    $this->load->view('head');
    $this->load->view('modificarform',$data);
    //$this->load->view('footer');
  }
  
  public function modificardb()
  {
    $idUsuario=$_POST['idUsuario'];
    $ps = array('nombre' =>$this->input->post('nombre'),'apellidoP' =>$this->input->post('apellidoP'),'apellidoM' =>$this->input->post('apellidoM'),'cuenta' =>$this->input->post('cuenta'),'contrasenia' =>$this->input->post('contrasenia'));
    

    $this->estudiantes_model->modificarU($idUsuario,$ps);
    $this->load->view('head');
    $this->load->view('modificarmensaje',$ps);
    //$this->load->view('footer');
  //  redirect('paises/index','refresh');
  }
   
   public function eliminardb()
  {
      
   

    $idperfilUsuario=$_POST['idUsuario'];
    $nombre=$_POST['nombre'];

    $ps = array('estado' =>0);
    $data['nombres']=$nombre;
    $this->estudiantes_model->eliminarU($idperfilUsuario,$ps);
    $this->load->view('head');
    $this->load->view('eliminarmensaje',$data);
    //$this->load->view('footer');
  }

    public function contenido()//ver contenido
  {
        $data['contenido']=$this->estudiantes_model->retornarCo();  
        
              $this->load->view('head');
               $this->load->view('vistacontenido',$data);
       

  }
    public function agregarC()//agregar contenido
  {
    
    
    $this->load->view('head');
    $this->load->view('agregarCform');
    //$this->load->view('footer');
  }
    public function agregarCdb()
  {
    
    //$pais=$_POST['pais'];
    //$data['pais']=$pais;
    //$this->paises_model->agregarPais($data);
    $c = array('idcontenido' =>$this->input->post('idcontenido'),'nombreArea' =>$this->input->post('nombreArea'), 'descripcion' =>$this->input->post('descripcion'));
         $this->estudiantes_model->agregarC($c);
    $this->load->view('head');
    $this->load->view('agregarCmensaje',$c);
   // $this->load->view('footer');

  }
  public function modificarC()
  {
    $idcontenido=$_POST['idcontenido'];
    $data['nombreArea']=$this->estudiantes_model->recuperarC($idcontenido);
    $this->load->view('head');
    $this->load->view('modificarCform',$data);
    //$this->load->view('footer');
  }
   public function modificarCdb()
  {
    $idcontenido=$_POST['idcontenido'];
    $c = array('idcontenido' =>$this->input->post('idcontenido'),'nombreArea' =>$this->input->post('nombreArea'),'descripcion' =>$this->input->post('descripcion'));
    

    $this->estudiantes_model->modificarC($idcontenido,$c);
    $this->load->view('head');
    $this->load->view('modificarCmensaje',$c);
    //$this->load->view('footer');
  //  redirect('paises/index','refresh');
  }

  public function eliminarCdb()
  {
      


    $idcontenido=$_POST['idcontenido'];
    $nombreArea=$_POST['nombreArea'];
    $data['nombreArea']=$nombreArea;
    $this->estudiantes_model->eliminarC($idcontenido);
    $this->load->view('head');
    $this->load->view('eliminarCmensaje',$data);
    //$this->load->view('footer');
  }
}
